# Tanguy ARNOULT

*Développeur web et bases de données*

# Etudes

- BTS Service Informatiques Aux Organisations (2022)
- BAC S Mathématiques - Mention très bien (2020)
- Validation du Cambridge - Niveau C2 (2020)
- Certicat Voltaire - 907 points / 1000 (2020)

# Compétences

- Création, Maintien et exploitation de base de données relationnelles
- Développement web (PHP, Js, HTML, CSS)
- Développement logiciel (C, Java, Lolo, C#)

# Expériences

- Résponsable informatique pour la médecine du travail (2022-...)
- Recruteur de donateurs (Été 2022)
- Stage en développement mobile pour Mobile Developpement (8 semaines en 2022)
- Stage de développement web pour la CC Chinon Vienne et Loire (6 semaines en 2021)
- Réserviste pour l'Armée de Terre, spécialisation NRBC (2016-2018) 

# Langues parlées

- Français (Maternelle)
- Anglais (Fluide)
- Russe (Langage courant)

# Annexe

- Permis de conduire